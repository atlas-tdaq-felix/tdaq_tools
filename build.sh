#!/bin/sh
set -e

usage()
{
  echo "usage: $(basename $0) <binary_tag> <destination_dir>"
  exit 1
}

if [ $# -lt 2 ]; then
    usage
fi

BINARY_TAG=${1}
DST_DIR=${2}

patchelf=external/patchelf/${FELIX_PATCHELF_VERSION}/${BINARY_TAG}/bin/patchelf
if [ ! -x "$patchelf" ]
then
  patchelf=../${patchelf}
fi


SRC=${TDAQ_BASE}/tdaq/${TDAQ_VERSION}/installed/${BINARY_TAG}

BIN=${DST_DIR}/bin
LIB=${DST_DIR}/lib

mkdir -p ${BIN}
mkdir -p ${LIB}

cp ${SRC}/bin/menuRCDTtcvi ${BIN}
cp ${SRC}/lib/libRCDTtc.so ${LIB}
cp ${SRC}/lib/libcmdline.so ${LIB}
cp ${SRC}/lib/libRCDVme.so ${LIB}
cp ${SRC}/lib/libRCDMenu.so ${LIB}
cp ${SRC}/lib/libRCDUtilities.so ${LIB}
cp ${SRC}/lib/libvme_rcc.so ${LIB}
cp ${SRC}/lib/libio_rcc.so ${LIB}
cp ${SRC}/lib/libcmem_rcc.so ${LIB}
cp ${SRC}/lib/librcc_time_stamp.so ${LIB}
cp ${SRC}/lib/libDFDebug.so ${LIB}
cp ${SRC}/lib/librcc_error.so ${LIB}

${patchelf} --set-rpath '$ORIGIN/../lib' --force-rpath ${BIN}/menuRCDTtcvi
