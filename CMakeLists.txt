cmake_minimum_required(VERSION 3.5)

set(PACKAGE tdaq_tools)
set(PACKAGE_VERSION 0.0.1)
set(PACKAGE_ARGS NO_HEADERS)

include(FELIX)

# Check to see if TDAQ_BASE is not explicitly set to nowhere (no access to TDAQ - felix-build-image for instance)
if (NOT $ENV{TDAQ_BASE} STREQUAL "/non-existing")
add_custom_command(
    OUTPUT menuRCDTtcvi
    COMMAND ./build.sh ${BINARY_TAG} ${CMAKE_CURRENT_BINARY_DIR}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
)
add_custom_target(MENU_RCDT_TCVI ALL DEPENDS menuRCDTtcvi)
endif()
